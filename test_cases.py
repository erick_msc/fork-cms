import unittest, time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as expect

class TestFunctions:

    def navigate_to(self, driver, section_text):
        """Handles the primary navigation on the left"""
        navigation = driver.find_element_by_css_selector('.main-navigation .navbar-nav')
        navigation.find_element_by_xpath('.//a[normalize-space()="' + section_text + '"]').click()

    def sub_navigate(self,driver, subsection_text):
        """Handles sub navigation menu in 'Modules' section"""
        navigation = driver.find_element_by_css_selector('.sub-navigation .navbar-nav')
        navigation.find_element_by_xpath('.//a[normalize-space()="' + subsection_text + '"]').click()

    def enter_main_content(self, driver, text_entry):
        iframe = driver.find_element_by_css_selector('#cke_text iframe')
        driver.switch_to_frame(iframe)
        driver.find_element_by_tag_name('body').send_keys(text_entry)
        driver.switch_to_default_content()

class SimpleTestCase(unittest.TestCase):

    demo_url = 'https://demo.fork-cms.com/private/'
    demo_user = 'demo@fork-cms.com'
    demo_pass = 'demo'
    common = TestFunctions()

    def setUp(self):
        """Call before every test case."""
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(4)

        # Get the login page and then login
        self.driver.get(self.demo_url)
        self.driver.find_element_by_id('backendEmail').send_keys(self.demo_user)
        self.driver.find_element_by_id('backendPassword').send_keys(self.demo_pass)
        self.driver.find_element_by_name('login').click()

    def tearDown(self):
        """Call after every test case."""
        self.driver.quit()

    def test_login(self):
        assert self.driver.title == 'Dashboard - My website - Fork CMS'
        time.sleep(2) # For demo purposes

    def test_create_draft_blog(self):

        blog_title = 'My Draft Blog ' + str(int(time.time()))

        # Navigate towards the Blog section
        self.common.navigate_to(self.driver, 'Modules')
        self.common.sub_navigate(self.driver, 'Blog')

        # Enter in some basic blog details
        self.driver.find_element_by_xpath('.//a[normalize-space()="Add article"]').click()
        self.driver.find_element_by_css_selector('input#title').send_keys(blog_title)
        self.common.enter_main_content(self.driver, 'Some new Text String')

        # And submit the form
        self.driver.find_element_by_css_selector("button[title='Save draft']").click()
        self.driver.find_element_by_class_name("fa-info")
        new_url_element = self.driver.find_element_by_css_selector('form .site-url')
        new_url = new_url_element.text

        # Check that we have a unique page for our blog and that it's a 404
        assert '/modules/blog/detail/' in new_url and not(new_url.endswith('/detail/'))
        new_url_element.click()
        element = WebDriverWait(self.driver, 10).until(
            expect.presence_of_element_located((By.XPATH, './/h1[normalize-space()="404"]')))
        time.sleep(2) # For demo purposes

    def test_create_published_blog(self):

        blog_title = 'My Published Blog ' + str(int(time.time()))

        # Navigate towards the Blog section
        self.common.navigate_to(self.driver, 'Modules')
        self.common.sub_navigate(self.driver, 'Blog')

        # Enter in some basic blog details
        self.driver.find_element_by_xpath('.//a[normalize-space()="Add article"]').click()
        self.driver.find_element_by_css_selector('input#title').send_keys(blog_title)
        self.common.enter_main_content(self.driver, 'Some new Text String')

        # And submit the form
        self.driver.find_element_by_css_selector("button[title='Publish']").click()

        # This should bring us back to the Blog listing page
        # So we go back in and check there is a published URL
        self.driver.find_element_by_xpath('.//a[normalize-space()="' + blog_title + '"]').click()
        new_url_element = self.driver.find_element_by_css_selector('form .site-url')
        new_url = new_url_element.text

        # Check that we have a unique page for our blog and that it's a 404
        assert '/modules/blog/detail/' in new_url and not(new_url.endswith('/detail/'))
        new_url_element.click()
        element = WebDriverWait(self.driver, 10).until(
            expect.presence_of_element_located((By.XPATH, './/h1[normalize-space()="' + blog_title + '"]')))
        time.sleep(2) # For demo purposes

if __name__ == '__main__':
    """Used for demo purposes"""
    unittest.main()